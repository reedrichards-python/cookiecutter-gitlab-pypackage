# -*- coding: utf-8 -*-

"""Main module."""


def hello(name: str) -> str:
    """
    Say hello
    """
    return "Hello " + name
