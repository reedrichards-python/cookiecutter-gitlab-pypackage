{% set is_open_source = cookiecutter.open_source_license != 'Not open source' -%}
{% for _ in cookiecutter.project_name %}={% endfor %}
{{ cookiecutter.project_name }}
{% for _ in cookiecutter.project_name %}={% endfor %}

{% if is_open_source %}
.. image:: https://img.shields.io/pypi/v/{{ cookiecutter.project_slug }}.svg
        :target: https://pypi.python.org/pypi/{{ cookiecutter.project_slug }}
        :alt: pypi

.. image:: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}/badges/master/coverage.svg?job=coverage
        :target: https://{{ cookiecutter.gitlab_username }}.gitlab.io/{{ cookiecutter.project_slug }}/coverage/index.html
        :alt: coverage

.. image:: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}/badges/master/pipeline.svg
        :alt: pipeline

.. image:: https://img.shields.io/pypi/l/gitlab-helper.svg
        :target: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}/raw/master/LICENSE
        :alt: PyPI - License

.. image:: https://img.shields.io/pypi/dm/{{ cookiecutter.project_slug }}.svg
        :alt: PyPI - Downloads

.. image:: https://img.shields.io/pypi/pyversions/{{ cookiecutter.project_slug }}.svg
        :alt: PyPI - Python Version

.. image:: https://img.shields.io/pypi/status/{{ cookiecutter.project_slug }}.svg
        :alt: PyPI - Status

{%- endif %}



{{ cookiecutter.project_short_description }}

{% if is_open_source %}
* Free software: {{ cookiecutter.open_source_license }}
* Documentation: https://{{ cookiecutter.gitlab_username }}.gitlab.io/{{ cookiecutter.project_slug  }}/index.html
* Coverage: https://{{ cookiecutter.gitlab_username }}.gitlab.io.gitlab.io/{{ cookiecutter.project_slug  }}/coverage/index.html
* Gitlab: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug  }}
{% endif %}

Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
