tox==3.14.0
pylint
coverage==4.5.4
{% if cookiecutter.use_pytest == 'y' -%}
pytest==4.6.5
pytest-dotenv
pytest-runner==5.1{% endif %}
